#Productivity Tracker

##Overview
Productivity Tracker is the best way to track your productivity over a period of time. It will count your keystrokes and clicks and will display it on a graph for you. If you spend most of your time watching YouTube, it will show that you didn't do much work that day. If you type out your entire 20 page English paper in 2 hours, it will show that you were very productive that day.

##Features:
* keystroke counter
* click counter
* graphs your productivity on a graph, all you have to do is click on the extension icon
* see your productivity between specific dates
* light weight and simplistic design
